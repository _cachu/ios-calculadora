// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace iosCalculadora
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn0 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn1 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn2 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn3 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn4 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn5 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn6 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn7 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn8 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btn9 { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnac { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btndiv { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btneq { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnminus { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnmult { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnneg { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnplus { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnpoint { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton btnporcentaje { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel txtGlobal { get; set; }

		[Action ("Btn0_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn0_TouchUpInside (UIButton sender);

		[Action ("Btn1_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn1_TouchUpInside (UIButton sender);

		[Action ("Btn2_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn2_TouchUpInside (UIButton sender);

		[Action ("Btn3_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn3_TouchUpInside (UIButton sender);

		[Action ("Btn4_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn4_TouchUpInside (UIButton sender);

		[Action ("Btn5_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn5_TouchUpInside (UIButton sender);

		[Action ("Btn6_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn6_TouchUpInside (UIButton sender);

		[Action ("Btn7_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn7_TouchUpInside (UIButton sender);

		[Action ("Btn8_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn8_TouchUpInside (UIButton sender);

		[Action ("Btn9_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btn9_TouchUpInside (UIButton sender);

		[Action ("Btnac_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnac_TouchUpInside (UIButton sender);

		[Action ("Btndiv_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btndiv_TouchUpInside (UIButton sender);

		[Action ("Btneq_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btneq_TouchUpInside (UIButton sender);

		[Action ("Btnminus_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnminus_TouchUpInside (UIButton sender);

		[Action ("Btnmult_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnmult_TouchUpInside (UIButton sender);

		[Action ("Btnneg_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnneg_TouchUpInside (UIButton sender);

		[Action ("Btnplus_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnplus_TouchUpInside (UIButton sender);

		[Action ("Btnpoint_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnpoint_TouchUpInside (UIButton sender);

		[Action ("Btnporcentaje_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void Btnporcentaje_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (btn0 != null) {
				btn0.Dispose ();
				btn0 = null;
			}
			if (btn1 != null) {
				btn1.Dispose ();
				btn1 = null;
			}
			if (btn2 != null) {
				btn2.Dispose ();
				btn2 = null;
			}
			if (btn3 != null) {
				btn3.Dispose ();
				btn3 = null;
			}
			if (btn4 != null) {
				btn4.Dispose ();
				btn4 = null;
			}
			if (btn5 != null) {
				btn5.Dispose ();
				btn5 = null;
			}
			if (btn6 != null) {
				btn6.Dispose ();
				btn6 = null;
			}
			if (btn7 != null) {
				btn7.Dispose ();
				btn7 = null;
			}
			if (btn8 != null) {
				btn8.Dispose ();
				btn8 = null;
			}
			if (btn9 != null) {
				btn9.Dispose ();
				btn9 = null;
			}
			if (btnac != null) {
				btnac.Dispose ();
				btnac = null;
			}
			if (btndiv != null) {
				btndiv.Dispose ();
				btndiv = null;
			}
			if (btneq != null) {
				btneq.Dispose ();
				btneq = null;
			}
			if (btnminus != null) {
				btnminus.Dispose ();
				btnminus = null;
			}
			if (btnmult != null) {
				btnmult.Dispose ();
				btnmult = null;
			}
			if (btnneg != null) {
				btnneg.Dispose ();
				btnneg = null;
			}
			if (btnplus != null) {
				btnplus.Dispose ();
				btnplus = null;
			}
			if (btnpoint != null) {
				btnpoint.Dispose ();
				btnpoint = null;
			}
			if (btnporcentaje != null) {
				btnporcentaje.Dispose ();
				btnporcentaje = null;
			}
			if (txtGlobal != null) {
				txtGlobal.Dispose ();
				txtGlobal = null;
			}
		}
	}
}
