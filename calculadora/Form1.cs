﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using LogicaCalculadora;

namespace calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            ops.global = txtGlobal.Text;
        }

        Operaciones ops = new Operaciones();
        private void btnplus_Click(object sender, EventArgs e)
        {
            ops.global = "0";
            var valor = 0.0;
            try
            {
                valor = ops.Suma(double.Parse(txtGlobal.Text));
            }
            catch (Exception ex)
            {
                valor = 0.0;
            }
            txtGlobal.Text = (valor.ToString());
        }

        private void btnminus_Click(object sender, EventArgs e)
        {
            ops.global = "0";
            var valor = 0.0;
            try
            {
                valor = ops.Resta(double.Parse(txtGlobal.Text));
            }
            catch (Exception ex)
            {
                valor = 0.0;
            }
            txtGlobal.Text = (valor.ToString());
        }

        private void btnmult_Click(object sender, EventArgs e)
        {
            ops.global = "0";
            var valor = 0.0;
            try
            {
                valor = ops.Multiplica(double.Parse(txtGlobal.Text));
            }
            catch (Exception ex)
            {
                valor = 0.0;
            }
            txtGlobal.Text = (valor.ToString());
        }

        private void btndiv_Click(object sender, EventArgs e)
        {
            ops.global = "0";
            var valor = 0.0;
            try
            {
                valor = ops.Divide(double.Parse(txtGlobal.Text));
            }
            catch (Exception ex)
            {
                valor = 0.0;
            }
            txtGlobal.Text = (valor.ToString());
        }

        private void btneq_Click(object sender, EventArgs e)
        {
            txtGlobal.Text = ops.Igualar(double.Parse(txtGlobal.Text)).ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtGlobal.Text = "0";
            ops.Vaciar();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (double.Parse(txtGlobal.Text) > 0)
            {
                txtGlobal.Text = "-" + txtGlobal.Text;
            }
            else
            {
                txtGlobal.Text = txtGlobal.Text.Replace("-", "");
            }
        }

        private void btn1_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }
        void reemplaza(string txt)
        {
            if (ops.global == "0")
            {
                ops.global = "";
            }
            txtGlobal.Text = ops.global + txt;
            ops.global = txtGlobal.Text;
            ops.GuardaSegundo(double.Parse(ops.global));
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            ops.global = ops.Porcentaje(double.Parse(ops.global)).ToString();
            txtGlobal.Text = ops.global;
        }

        private void btnpoint_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            if (!ops.global.Contains(text.Text))
                reemplaza(text.Text);
        }

        private void btn0_Click(object sender, EventArgs e)
        {
            var text = sender as Button;
            reemplaza(text.Text);
        }
    }
}
