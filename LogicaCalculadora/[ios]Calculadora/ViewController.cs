﻿using System;
using LogicaCalculadora;
using UIKit;

namespace iosCalculadora
{
	public partial class ViewController : UIViewController
	{
		Operaciones ops = new Operaciones();
		public ViewController (IntPtr handle) : base (handle)
		{
			
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			ops.global = txtGlobal.Text;
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		void reemplaza(string txt)
		{
			btnac.SetTitle("C",UIControlState.Normal);
			ops.bloquearSuma = false;
			ops.bloquearDivide = false;
			ops.bloquearMultiplica = false;
			ops.bloquearResta = false;
			if (ops.global == "0") {
				ops.global = "";
			}
			var length = 9;
			if (ops.countRestr==true) {
				if (txtGlobal.Text.Contains ("."))
					length = 10;
				if (ops.global.Length < length) {
					txtGlobal.Text = ops.global + txt;
					ops.global = txtGlobal.Text;
					ops.GuardaSegundo (0 +double.Parse (ops.global));

				} else {
					if (ops.global != "") {
						ops.countRestr = false;
					} else {
						ops.countRestr = true;
					}
				}
			}

		
		}
		partial void Btn1_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn2_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn3_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn4_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn5_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn6_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn7_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn8_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btn9_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}

		partial void Btnplus_TouchUpInside (UIButton sender)
		{
			ops.global = "0";
			var valor = 0.0;
			try
			{
				valor = ops.Suma(double.Parse(txtGlobal.Text));
			}
			catch (Exception)
			{
				valor = 0.0;
			}
			txtGlobal.Text = (valor.ToString());
		}

		partial void Btnminus_TouchUpInside (UIButton sender)
		{
			ops.global = "0";
			var valor = 0.0;
			try
			{
				valor = ops.Resta(double.Parse(txtGlobal.Text));
			}
			catch (Exception)
			{
				valor = 0.0;
			}
			txtGlobal.Text = (valor.ToString());
		}

		partial void Btnmult_TouchUpInside (UIButton sender)
		{
			ops.global = "0";
			var valor = 0.0;
			try
			{
				valor = ops.Multiplica(double.Parse(txtGlobal.Text));
			}
			catch (Exception)
			{
				valor = 0.0;
			}
			txtGlobal.Text = (valor.ToString());
		}

		partial void Btndiv_TouchUpInside (UIButton sender)
		{
			ops.global = "0";
			var valor = 0.0;
			try
			{
				valor = ops.Divide(double.Parse(txtGlobal.Text));
			}
			catch (Exception)
			{
				valor = 0.0;
			}
			txtGlobal.Text = (valor.ToString());
		}

		partial void Btnpoint_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			if(txtGlobal.Text!=ops.global)
				ops.global="0";
			if (!ops.global.Contains(text.CurrentTitle)){
				if(ops.global=="0"){
					reemplaza("0"+text.CurrentTitle);
				}
				else
				reemplaza(text.CurrentTitle);
			}
		}

		partial void Btneq_TouchUpInside (UIButton sender)
		{
			txtGlobal.Text = ops.Igualar(double.Parse(txtGlobal.Text)).ToString();
		}

		partial void Btnporcentaje_TouchUpInside (UIButton sender)
		{
			ops.global = ops.Porcentaje(double.Parse(ops.global)).ToString();
			txtGlobal.Text = ops.global;
		}

		partial void Btnneg_TouchUpInside (UIButton sender)
		{
			if (double.Parse(txtGlobal.Text) > 0)
			{
				txtGlobal.Text = "-" + txtGlobal.Text;

			}
			else
			{
				txtGlobal.Text = txtGlobal.Text.Replace("-", "");
			}
			ops.global=txtGlobal.Text;
		}

		partial void Btnac_TouchUpInside (UIButton sender)
		{
			btnac.SetTitle("AC",UIControlState.Normal);
			txtGlobal.Text = "0";
			ops.Vaciar();
		}

		partial void Btn0_TouchUpInside (UIButton sender)
		{
			var text = sender as UIButton;
			reemplaza(text.CurrentTitle);
		}
}
}
