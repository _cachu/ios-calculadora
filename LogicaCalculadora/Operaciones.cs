﻿using System;
using System.Collections.Generic;

namespace LogicaCalculadora
{
    public class Operaciones
    {
        public double[] lista;
        public string simbolo;
        public string global;
		public double global2;
		public bool bloquearSuma;
		public bool bloquearResta;
		public bool bloquearMultiplica;
		public bool bloquearDivide;
		public bool countRestr;
        public List<string> jerarquia;
        public List<double> numeros;
        public List<string> orden = new List<string> { "/", "*", "+", "-" };

        public Operaciones()
        {
            lista = new double[2];
            jerarquia = new List<string>();
            numeros = new List<double>();
			countRestr = true;
        }
        public double operacion(double op)
        {
			numeros.Add(lista[1]);
			if (numeros.Count == 1) {
				return op;
			} 
            else
            {
                var resultado = 0.0;
                for (int counter = 0; counter < jerarquia.Count; counter++)
                {
                    if (counter == 0)
                    {
                        resultado = numeros[0];
                        continue;
                    }
                    var simbolo2 = jerarquia[counter-1];
                    switch (simbolo2)
                    {
                        case "+":
                            resultado = resultado + numeros[counter];
                            break;
                        case "-":
                            resultado = resultado - numeros[counter];
                            break;
                        case "*":
                            resultado = resultado * numeros[counter];
                            break;
                        case "/":
                            resultado = resultado / numeros[counter];
                            break;
                    }
                }
				global2 = resultado;
                return resultado;
            }
        }
        public double Suma(double op)
        {
			if (!bloquearSuma) {
				bloquearSuma = true;
				GuardaSimbolo ("+");
				return operacion (op);
			} else {
				return op;
			}
        }
        public double Resta(double op)
        {
			if (!bloquearResta) {
				bloquearResta = true;
				GuardaSimbolo ("-");
				return operacion (op);
			} else {
				return op;
			}
        }
        public double Multiplica(double op)
        {
			if (!bloquearMultiplica) {
				bloquearMultiplica = true;
				GuardaSimbolo ("*");
				return operacion (op);
			} else {
				return op;
			}
        }
        public double Divide(double op)
        {
			if (!bloquearDivide) {
				bloquearDivide = true;
				GuardaSimbolo ("/");
				return operacion (op);
			} else {
				return op;
			}
        }
        public double Igualar(double valor = 0)
        {
			countRestr = true;
			bloquearSuma = false;
			bloquearResta = false;
			bloquearMultiplica = false;
			bloquearDivide = false;

			if (numeros.Count == 0) {
				return lista[1];
			}
			var numcount = numeros.Count;
            if (numcount > 1)
            {				
				if (numeros.Count == jerarquia.Count)
					numeros.Add(lista[1]);
				if (jerarquia.Count == 0) {
					jerarquia.Add (simbolo);
					numeros[1]= (global2);
				}
                var posicion = 4;
                var simbolo2 = "";
                numeros.Add(valor);
                for (int counter = 0; counter < jerarquia.Count + 1; counter++)
                {
                    var item = "";
                    try
                    {
                        item = jerarquia[counter];
                    }
                    catch (Exception)
                    {
                        try
                        {
                            item = jerarquia[counter - 1];
                        }
                        catch (Exception)
                        {
                            continue;
                        }
                    }
                    var index = orden.IndexOf(item);
                    if (index < posicion)
                    {
                        simbolo2 = item;
                    }
                    else
                    {
                        if (index == posicion)
                        {
                            simbolo2 = item;
                        }
                        if (jerarquia.Count > 1)
                        {
                            posicion = jerarquia.IndexOf(simbolo2);
                        }
                        else
                        {
                            posicion = 0;
                            simbolo2 = jerarquia[posicion];
                        }
                        switch (simbolo2)
                        {
                            case "+":
                                numeros[posicion] = numeros[posicion] + numeros[posicion + 1];
                                break;
                            case "-":
                                numeros[posicion] = numeros[posicion] - numeros[posicion + 1];
                                break;
                            case "*":
                                numeros[posicion] = numeros[posicion] * numeros[posicion + 1];
                                break;
                            case "/":
                                numeros[posicion] = numeros[posicion] / numeros[posicion + 1];
                                break;
                        }

                        jerarquia.RemoveAt(posicion);
                        numeros.RemoveAt(posicion + 1);
                        counter = -1;
                        posicion = 4;
                    }
                    posicion = orden.IndexOf(item);
                }
				var resultado = numeros [0];
				Vaciar ();
                return resultado;
            }
            else
            {
				if (numeros.Count == jerarquia.Count)
					numeros.Add(lista[1]);
                var resultado = 0.0;
                switch (simbolo)
                {
                    case "+":
                        resultado = numeros[0] + lista[1];
                        break;
                    case "-":
                        resultado = numeros[0] - lista[1];
                        break;
                    case "*":
                        resultado = numeros[0] * lista[1];
                        break;
                    case "/":
                        resultado = numeros[0] / lista[1];
                        break;
                }
				if (numeros.Count > 0) {
					numeros [0] = resultado;
					if (numeros.Count > 1) {
						numeros.RemoveAt (1);
					}
				}
                else
                {
                    resultado = double.Parse(global);
                }
				if (jerarquia.Count > 0) {
					jerarquia.RemoveAt (0);
				}
                return resultado;
            }
        }
        public double Porcentaje(double valor)
        {
			var res=Igualar (valor);
			lista [1] = res/100;
			return lista [1];
        }

        public void GuardaPrimero(double valor)
        {
            lista[0] = valor;
        }
        public void GuardaSegundo(double valor)
        {
            lista[1] = valor;
        }
        public void GuardaSimbolo(string valor)
        {
			countRestr = true;
            simbolo = valor;
            jerarquia.Add(valor);
        }

        public void Vaciar()
        {
            simbolo = "";
            lista[0] = 0;
            lista[1] = 0;
            global = "0";
            jerarquia.Clear();
            numeros.Clear();
        }
    }
}

